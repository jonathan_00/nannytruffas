package br.com.nannytruffas.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.nannytruffas.model.Login;
import br.com.nannytruffas.utils.utils;

public class LoginDao {
	public Login buscarUsuario(String usuario, String senha) {
		Login Usuario;

		String jpql = "SELECT DISTINCT u FROM Login u  WHERE u.usuario = :pUsuario AND u.senha = :pSenha";

		EntityManager em = JPAUtil.getEntityManager();
		TypedQuery<Login> query = em.createQuery(jpql, Login.class);
		query.setParameter("pUsuario", usuario);
		query.setParameter("pSenha", utils.md5(senha));

		try {
			Usuario = query.getSingleResult();
		} catch (NoResultException ex) {
			Usuario = null;
		}

		em.close();
		
		return Usuario;
	}
}
