package br.com.nannytruffas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class IngredienteDaCompra implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//auto incremente
	private Integer id;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_compra == null) ? 0 : id_compra.hashCode());
		result = prime * result + ((id_ingrediente == null) ? 0 : id_ingrediente.hashCode());
		result = prime * result + ((quantidade == null) ? 0 : quantidade.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngredienteDaCompra other = (IngredienteDaCompra) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_compra == null) {
			if (other.id_compra != null)
				return false;
		} else if (!id_compra.equals(other.id_compra))
			return false;
		if (id_ingrediente == null) {
			if (other.id_ingrediente != null)
				return false;
		} else if (!id_ingrediente.equals(other.id_ingrediente))
			return false;
		if (quantidade == null) {
			if (other.quantidade != null)
				return false;
		} else if (!quantidade.equals(other.quantidade))
			return false;
		return true;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Ingredientes getId_ingrediente() {
		return id_ingrediente;
	}
	public void setId_ingrediente(Ingredientes id_ingrediente) {
		this.id_ingrediente = id_ingrediente;
	}
	public Compra getId_compra() {
		return id_compra;
	}
	public void setId_compra(Compra id_compra) {
		this.id_compra = id_compra;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	@ManyToOne
	@JoinColumn(name="ing_id")
	private Ingredientes id_ingrediente;
	
	@ManyToOne
	@JoinColumn(name="compra_id")
	private Compra id_compra;
	private Integer quantidade;
	
	
	
	
	
}
