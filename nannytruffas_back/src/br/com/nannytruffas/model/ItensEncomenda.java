package br.com.nannytruffas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ItensEncomenda implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//auto incremente
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="prod_id")
	private Produto id_produto;
	
	@ManyToOne
	@JoinColumn(name="enc_id")
	private Encomenda id_encomenda;
	
	private Integer quantidade;
	private Double subtotal;
	
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_encomenda == null) ? 0 : id_encomenda.hashCode());
		result = prime * result + ((id_produto == null) ? 0 : id_produto.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItensEncomenda other = (ItensEncomenda) obj;
		if (id_encomenda == null) {
			if (other.id_encomenda != null)
				return false;
		} else if (!id_encomenda.equals(other.id_encomenda))
			return false;
		if (id_produto == null) {
			if (other.id_produto != null)
				return false;
		} else if (!id_produto.equals(other.id_produto))
			return false;
		return true;
	}
	
	
	
	
	
	
	
}
