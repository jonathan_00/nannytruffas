package br.com.nannytruffas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Produto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//auto incremente
	@Column(name="prod_id")
	private Integer id;
	private String nome;
	private Double preco_unitario;
	private Double preco_cento;
	
	@ManyToOne
	private TipoProduto Tipo = new TipoProduto();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getPreco_unitario() {
		return preco_unitario;
	}

	public void setPreco_unitario(Double preco_unitario) {
		this.preco_unitario = preco_unitario;
	}

	public Double getPreco_cento() {
		return preco_cento;
	}

	public void setPreco_cento(Double preco_cento) {
		this.preco_cento = preco_cento;
	}

	public TipoProduto getTipo() {
		return Tipo;
	}

	public void setTipo(TipoProduto tipo) {
		Tipo = tipo;
	}
	
	
}
