package br.com.nannytruffas.model;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Feedback {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//auto incremente
	@Column(name="feed_id")
	private Integer id;
	private String email;
	private String mensagem;
	
	@Temporal(TemporalType.DATE)
	private Calendar data_publicacao = Calendar.getInstance();
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Calendar getData_publicacao() {
		return data_publicacao;
	}
	public void setData_publicacao(Calendar data_publicacao) {
		this.data_publicacao = data_publicacao;
	}
	
	
	
	
}
