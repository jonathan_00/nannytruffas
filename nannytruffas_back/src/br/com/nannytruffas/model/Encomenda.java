package br.com.nannytruffas.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Encomenda {	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//auto incremente
	@Column(name="enc_id")
	private Integer id;
	private String email;
	private String nome;
	private String telefone;
	private String celular;
	private String pagamento;
	private Double total;
	
	private Calendar data_encomenda = Calendar.getInstance();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getPagamento() {
		return pagamento;
	}

	public void setPagamento(String pagamento) {
		this.pagamento = pagamento;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Calendar getData_encomenda() {
		return data_encomenda;
	}

	public void setData_encomenda(Calendar data_encomenda) {
		this.data_encomenda = data_encomenda;
	}

	public Calendar getData_entrega() {
		return data_entrega;
	}

	public void setData_entrega(Calendar data_entrega) {
		this.data_entrega = data_entrega;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	private Calendar data_entrega = Calendar.getInstance();
	
	private String endereco;


}
