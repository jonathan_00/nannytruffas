package br.com.nannytruffas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Ingredientes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//auto incremente
	@Column(name="ing_id")
	private Integer id;
	private String nome;
	private Integer quantidade;
	
	
	@ManyToOne
	private UnidadeMedida unidade = new UnidadeMedida();
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public UnidadeMedida getUnidade() {
		return unidade;
	}
	public void setUnidade(UnidadeMedida unidade) {
		this.unidade = unidade;
	}
	
	
}
