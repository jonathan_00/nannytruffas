package br.com.nannytruffas.controller;

import java.util.List;

import br.com.nannytruffas.dao.Dao;
import br.com.nannytruffas.model.Feedback;

public class FeedbackController {
	public List<Feedback> getTodosFeedbacks(){
		return new Dao<Feedback>(Feedback.class).listaTodos();
	}
}
