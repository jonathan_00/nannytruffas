package br.com.nannytruffas.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.nannytruffas.dao.Dao;
import br.com.nannytruffas.model.Encomenda;

@ManagedBean
@ViewScoped
public class EncomendaController {
	
	Encomenda encomenda = new Encomenda();

	public Encomenda getEncomenda() {
		return encomenda;
	}

	public void setEncomenda(Encomenda encomenda) {
		this.encomenda = encomenda;
	}
	
	
	public void gravar(){
		Dao<Encomenda> dao = new Dao<Encomenda>(Encomenda.class);
		
		if (encomenda.getId() == null)
			dao.adiciona(encomenda);
		else
			dao.atualiza(encomenda);
		
		encomenda = new Encomenda();
	}
	
	
	public List<Encomenda> getTodasEncomendas(){
		return new Dao<Encomenda>(Encomenda.class).listaTodos();
	}
	
	public void carregar(Encomenda encomenda){
		this.encomenda = encomenda;
	}
	
	public void remover(Encomenda encomenda){
		new Dao<Encomenda>(Encomenda.class).remove(encomenda.getId());
	}

}
