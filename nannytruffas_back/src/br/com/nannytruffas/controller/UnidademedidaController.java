package br.com.nannytruffas.controller;

import java.util.List;

import br.com.nannytruffas.dao.Dao;
import br.com.nannytruffas.model.UnidadeMedida;

public class UnidademedidaController {
	public List<UnidadeMedida> getTodasUnidadeMedida(){
		return new Dao<UnidadeMedida>(UnidadeMedida.class).listaTodos();
	}
}
