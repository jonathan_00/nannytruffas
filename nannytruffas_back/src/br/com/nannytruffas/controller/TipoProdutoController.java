package br.com.nannytruffas.controller;

import java.util.List;

import br.com.nannytruffas.dao.Dao;
import br.com.nannytruffas.model.TipoProduto;

public class TipoProdutoController {
	public List<TipoProduto> getTodosTipoProduto(){
		return new Dao<TipoProduto>(TipoProduto.class).listaTodos();
	}
}
