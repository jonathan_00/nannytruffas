package br.com.nannytruffas.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.nannytruffas.dao.Dao;
import br.com.nannytruffas.model.Produto;
import br.com.nannytruffas.model.TipoProduto;


@ManagedBean
@ViewScoped
public class ProdutoController {
	private Produto prod = new Produto();
	private Integer categoriaId;
	
	public void gravar(){
		Dao<Produto> dao = new Dao<Produto>(Produto.class);
		
		TipoProduto p = new Dao<TipoProduto>(TipoProduto.class).listaPorId(categoriaId);
		prod.setTipo(p);
		
		if(prod.getId() == null){
			dao.adiciona(prod);
		}
		else
			dao.atualiza(prod);
		
		prod = new Produto();
		categoriaId = null;
	}
	
	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public void atualiza(Produto p){
		System.out.println(p);
		this.prod = p;
	}
	
	public void remover(Produto p) {
		 new Dao<Produto>(Produto.class).remove(p.getId());
	}

	
	public Produto getProd() {
		return prod;
	}

	public void setProd(Produto prod) {
		this.prod = prod;
	}

	public List<TipoProduto> getTodosTipoProduto(){
		return new Dao<TipoProduto>(TipoProduto.class).listaTodos();
	}
	
	public List<Produto> getTodosProdutos(){
		return new Dao<Produto>(Produto.class).listaTodos();
	}
	
	
}
