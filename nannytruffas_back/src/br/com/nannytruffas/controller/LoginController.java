package br.com.nannytruffas.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.nannytruffas.dao.LoginDao;
import br.com.nannytruffas.model.Login;


@ManagedBean
@ViewScoped
public class LoginController {
	private Login login = new Login();

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}
	
	public String logar() {

		login = new LoginDao().buscarUsuario(login.getUsuario(),login.getSenha());

		FacesContext context = FacesContext.getCurrentInstance();

		if (this.login != null) {
			context.getExternalContext().getSessionMap().put("usuarioLogado", login);
			return "dashboard?faces-redirect=true";
			
		} else {
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage("Usuario e/ou senha incorretos"));
			
			return "login?faces-redirect=true";
		}
	}

	

}
