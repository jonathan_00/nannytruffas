package br.com.nannytruffas.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.nannytruffas.dao.Dao;
import br.com.nannytruffas.model.Imagens;


@ManagedBean
@ViewScoped
public class ImagensController {
	
	
	private Imagens imagem =  new Imagens();

	public Imagens getImagem() {
		return imagem;
	}

	public void setImagem(Imagens imagem) {
		this.imagem = imagem;
	}
	
	public void gravar(){
		Dao<Imagens> dao = new Dao<Imagens>(Imagens.class);
		
		if (imagem.getId() == null)
			dao.adiciona(imagem);
		else
			dao.atualiza(imagem);
		
		imagem = new Imagens();
	}
	
	public List<Imagens> getTodasImagens(){
		return new Dao<Imagens>(Imagens.class).listaTodos();
	}
	
	public void carregar(Imagens imagem){
		this.imagem = imagem;
	}
	
	public void remover(Imagens imagem){
		new Dao<Imagens>(Imagens.class).remove(imagem.getId());
	}
	

}
